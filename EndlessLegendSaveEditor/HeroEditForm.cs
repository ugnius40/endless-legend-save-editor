﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EndlessLegendSaveEditor
{

    public partial class HeroEditFm : Form
    {

        public List<HeroEntry> HeroList = new List<HeroEntry>();

        public HeroEditFm()
        {
            InitializeComponent();
        }

        public void SetHeroList(List<HeroEntry> newhlist) {
            HeroList.Clear();
            HeroList.AddRange(newhlist);
            HeroListUpdated();
        }

        public bool HeroDataChanged() { 
            foreach (HeroEntry hero in HeroList)
                if (hero.changed) return true;
            return false;
        }

        private void btClose_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void HeroListUpdated() {
            lbHeroeNames.Items.Clear();
            foreach (HeroEntry hero in HeroList)
                lbHeroeNames.Items.Add(hero.Name);
            if (lbHeroeNames.Items.Count > 0)
                lbHeroeNames.SelectedIndex = 0;
        }

        private void lbHeroeNames_SelectedIndexChanged(object sender, EventArgs e)
        {
            pgProperties.SelectedObject = HeroList[lbHeroeNames.SelectedIndex];
        }

    }

    public class HeroEntry : Object
    {
        public HeroEntry(XElement dnode, XElement unode)
        {
            designNode = dnode;
            unitNode = unode;
        }

        public bool changed = false;
            
        public XElement designNode;
        public XElement unitNode;

        private string GetProperty(string propname) {
            XElement property = unitNode.XPathSelectElement("SimulationObject/Properties/Property[@Name='" + propname + "']");
            if (property != null) return property.Value;
            return "";
        }

        private void SetProperty(string propname, string newvalue) {
            XElement property = unitNode.XPathSelectElement("SimulationObject/Properties/Property[@Name='" + propname + "']");
            if (property != null)
            {
                property.Value = newvalue;
                changed = true;
            }
            else {
                MessageBox.Show("Hero has no property named \"" + propname + "\", for example if hero has just been bought and has no experience yet.", "Setting Invalid Property");
            }
        }

        [DisplayName("Hero Name"), CategoryAttribute("Names"), DescriptionAttribute("Heroe Name"), ReadOnlyAttribute(true)]
        public string Name
        {
            get { return designNode.Element("UserDefinedName").Value; }
        }

        [DisplayName("Maximum Skill Points"), CategoryAttribute("Properties")]
        public int MaximumSkillPoints {
            get { return Convert.ToInt32(GetProperty("MaximumSkillPoints")); }
            set { SetProperty("MaximumSkillPoints", value.ToString()); }
        }

        [DisplayName("Experience"), CategoryAttribute("Properties")]
        public float Experience
        {
            get {
                try
                {
                    return Convert.ToSingle(GetProperty("Experience"));
                }
                catch (FormatException) {
                    return 0.0f;
                }
            }
            set { SetProperty("Experience", value.ToString()); }
        }
    }
}
