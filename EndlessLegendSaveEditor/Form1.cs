﻿using System;
using System.Globalization;
using System.Linq;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Diagnostics;

namespace EndlessLegendSaveEditor
{
  public partial class Form1 : Form
  {
    private static readonly string path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\Endless Legend\Save Files\"; // Save files folder location
    private const string file = "Endless Legend/Game.xml"; // Game data xml file location relative to save zip root
    private const string playerempirename = "Empire#0"; // Player empire name (for single player on slot 0)
    private XDocument gamedoc = null; // Stores Loaded Game data XML 
    private XElement plEmpireNode = null; // Points to XML node representing player empire
    private HeroEditFm HeroForm = new HeroEditFm();

    public Form1() // Form constructor
    {
      InitializeComponent();
    }

    private void Form1_Load(object sender, EventArgs e) // Called on finishing loading main form, initializes save list
    {
        RefreshSaveList();
    }

    private void RefreshSaveList() {
        var directoryInfo = new DirectoryInfo(path);
        if (!directoryInfo.Exists)
        { // Bail out if save path does not exist, leaving some info for user
            toolStripStatusLabel1.Text = "Save file folder not found!";
            return;
        }

        listBox1.Items.Clear();
        // Some linq magick i quite not understand, enumerates all zip files in a folder ordered by latest edited first
        foreach (string save in (from file in directoryInfo.GetFiles() where file.Extension.ToLower() == ".zip" orderby file.LastWriteTime descending select file.Name))
            listBox1.Items.Add(Path.GetFileNameWithoutExtension(save));

        // Inform user of save files found
        toolStripStatusLabel1.Text = "Found " + listBox1.Items.Count + " saves.";

        // Forces selection of first item, thus initiating XML parsing and reading property values
        if (listBox1.Items.Count > 0)
            listBox1.SelectedIndex = 0;
    }

    // Sets plEmpireNode variable to player empires XML node, empirename - passes Player empire name, usually "Empire#0"
    private XElement getPlayerEmpireNode(string empirename) {
        return gamedoc.XPathSelectElement("/GameManager/Game/Empires/MajorEmpire[@Name='" + empirename + "']");
    }

    // returns Player empires property XML Node by name passed in propName
    private XElement getEmpireProperty(string propName) {
        return plEmpireNode.XPathSelectElement("SimulationObject/Properties/Property[@Name='" + propName + "']");
    }

    // Event launches when item in Save selection box has changed. Loads up Game.xml file from zip, sets player empire node that of "Empire#0", reads and sets changeable property values to edit boxes.
    private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
    {
        gamedoc = GetXML(path + listBox1.SelectedItem + ".zip");
        if ((plEmpireNode = getPlayerEmpireNode(playerempirename)) != null) {
            textBox1.Text = getEmpireProperty("BankAccount").Value;
            textBox2.Text = getEmpireProperty("EmpirePointStock").Value;
            textBox3.Text = getEmpireProperty("EmpireResearchStock").Value;
            textBox4.Text = getEmpireProperty("EmpirePeacePointStock").Value;
        }
    }

    // Event, launches on "Update Save" button click, updates values in XML and saves to Zip save file, also makes a backup if option is selected
    private void button1_Click(object sender, EventArgs e)
    {
      // Validate inputs
      float f;
      if (!float.TryParse(textBox1.Text, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out f) ||
        !float.TryParse(textBox2.Text, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out f) ||
        !float.TryParse(textBox3.Text, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out f) ||
        !float.TryParse(textBox4.Text, NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out f))
      {
        toolStripStatusLabel1.Text = "Incorrect input.";
        return;
      }
      string zip = path + listBox1.SelectedItem + ".zip";
      
      // Make backup
      if (checkBox1.Checked)
          File.Copy(zip, path + listBox1.SelectedItem + ".zip.bak", true);
      
      // Apply main property changes
      getEmpireProperty("BankAccount").Value = textBox1.Text;
      getEmpireProperty("EmpirePointStock").Value = textBox2.Text;
      getEmpireProperty("EmpireResearchStock").Value = textBox3.Text;
      getEmpireProperty("EmpirePeacePointStock").Value = textBox4.Text;
      
      // Regexps for matching resource properties
      var rxStrat = new Regex("^Strategic\\d+Stock$", RegexOptions.IgnoreCase);
      var rxLuxury = new Regex("^Luxury\\d+Stock$", RegexOptions.IgnoreCase);
      
      var eprops = plEmpireNode.XPathSelectElements("SimulationObject/Properties/Property"); // Property list query
      if (checkBox2.Checked) { // Set Strategic resources to 999
          foreach (XElement stratRes in (from el in eprops where rxStrat.IsMatch(el.Attribute("Name").Value) select el))
              stratRes.Value = "999";          
      }

      if (checkBox3.Checked) { // Set Luxury resources to 999
          foreach (XElement luxuryRes in (from el in eprops where rxLuxury.IsMatch(el.Attribute("Name").Value) select el))
              luxuryRes.Value = "999";
      }

      if (checkBox4.Checked) { // Heal All Armies
          var armies = plEmpireNode.XPathSelectElement("Agencies/DepartmentOfDefense/Armies");
          foreach (XElement unithealth in (from unit in armies.Descendants("Unit")
                                           select unit.XPathSelectElement("SimulationObject/Properties/Property[@Name='Health']")))
              unithealth.Value = "1";
      }

      if (cbResetMovement.Checked) { // Reset All Movement Spent points to 0
          var armies = plEmpireNode.XPathSelectElement("Agencies/DepartmentOfDefense/Armies");
          foreach (XElement unitspentmoves in (from unit in armies.Descendants("Unit")
                                           select unit.XPathSelectElement("SimulationObject/Properties/Property[@Name='SpentMovement']")))
              unitspentmoves.Value = "0";
      }

      if (cbBoostersUp.Checked) { 
          foreach (XElement booster in plEmpireNode.XPathSelectElements("Agencies/DepartmentOfPlanificationAndDevelopment/Boosters/Booster")) {
              try {
                int bDuration = Convert.ToInt32(booster.Attribute("Duration").Value);
                int bRemaining = Convert.ToInt32(booster.Attribute("RemainingTime").Value);
                booster.Attribute("Duration").Value = "600";
                booster.Attribute("RemainingTime").Value = (bRemaining - bDuration + 600).ToString();
              } catch (FormatException) {
                  Debug.WriteLine("Error parsing Booster durations, values is not integer");
              }               
          }
      }

      // Zip Changes      
      using (ZipArchive archive = ZipFile.Open(zip, ZipArchiveMode.Update))
      {
          archive.GetEntry(file).Delete(); // Delete old Game.xml file
          using (StreamWriter writer = new StreamWriter(archive.CreateEntry(file).Open()))
              gamedoc.Save(writer); // Write changed Game.xml to savegame zip 
      }
      
      // Inform user of successful update
      toolStripStatusLabel1.Text = "Save \"" + listBox1.SelectedItem + "\" updated.";
    } // button1_Click -- Ends

    // Loads and parses zipped XML file to XmlDocument object and returns it  
    private static XDocument GetXML(string path)
    {
        using (ZipArchive archive = ZipFile.OpenRead(path))
        using (StreamReader s = new StreamReader(archive.GetEntry(file).Open()))
            return XDocument.Load(s);
    }

    private void btnReloadSaveList_Click(object sender, EventArgs e)
    {
        RefreshSaveList();
    }

    private void addHeroesInNode(XElement anode, List<HeroEntry> heroList) {
        foreach (XElement herounit in (from unit in anode.Descendants("Unit") where unit.Element("Rank").Attribute("Name").Value == "UnitRankHero" select unit))
        {
            XElement herodesign = plEmpireNode.XPathSelectElement("Agencies/DepartmentOfDefense/UnitDesigns/Hidden/UnitDesign[@Model='" + herounit.Attribute("UnitDesignModel").Value + "']");
            // Debug.WriteLine("UnitDesignModel (" + herounit.Attribute("UnitDesignModel").Value + ") Named: " + herodesign.Element("UserDefinedName").Value);
            heroList.Add(new HeroEntry(herodesign, herounit));
        }
    }

    private void btEditHeroes_Click(object sender, EventArgs e)
    {
        // Make Hero List:
        List<HeroEntry> heroList = new List<HeroEntry>();

        // Get Heroes in armies:
        addHeroesInNode(plEmpireNode.XPathSelectElement("Agencies/DepartmentOfDefense/Armies"), heroList);
        addHeroesInNode(plEmpireNode.XPathSelectElement("Agencies/DepartmentOfTheInterior/Cities"), heroList);
        addHeroesInNode(plEmpireNode.XPathSelectElement("Agencies/DepartmentOfEducation/UnassignedHeroes"), heroList);

        // Update Heroe List:
        HeroForm.SetHeroList(heroList);

        if (HeroForm.ShowDialog() == DialogResult.OK)
        {
            if (HeroForm.HeroDataChanged()) toolStripStatusLabel1.Text = "Heroes edited, press \"Update Save\"";
            Debug.WriteLine("All OK");
        }
    }
  }
}